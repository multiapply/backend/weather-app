RSpec::Matchers.define :be_a_url do
  match do |actual|
    actual =~ URI::DEFAULT_PARSER.make_regexp
  end
end